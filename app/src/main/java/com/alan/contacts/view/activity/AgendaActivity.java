package com.alan.contacts.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.alan.contacts.R;
import com.alan.contacts.application.Session;
import com.alan.contacts.model.Contact;
import com.alan.contacts.view.adapter.ContactAdapter;
import com.alan.contacts.view.adapter.SimpleSectionedRecyclerViewAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class AgendaActivity extends AppCompatActivity {

    private static AtomicBoolean lazyRefresh = new AtomicBoolean(false);
    private ContactAdapter contactAdapter;
    private RecyclerView recyclerView;
    private List<Contact> contactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);
        contactList = Session.getSession().getContactList();
        recyclerView = (RecyclerView) findViewById(R.id.contact_list_rv);

        sortContacts();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(layoutManager);
        contactAdapter = new ContactAdapter(getApplicationContext(), contactList, new ContactAdapter.ContactClickListener() {
            @Override
            public void onItemClick(Contact contact) {
                Intent intent = new Intent(AgendaActivity.this, ContactDetailsActivity.class);
                intent.putExtra("contact", new Gson().toJson(contact));
                startActivity(intent);
            }
        });

        //Your RecyclerView
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));

        defineSections(getFavorites(contactList));

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_agenda);
    }

    private void sortContacts() {
        Collections.sort(contactList, Contact.COMPARE_BY_NAME);
        Collections.sort(contactList, Contact.COMPARE_BY_FAVORITE);
    }

    private void defineSections(List<Contact> favoriteContactList) {
        List<SimpleSectionedRecyclerViewAdapter.Section> sections = new ArrayList<>();
        sections.add(new SimpleSectionedRecyclerViewAdapter.Section(0,getString(R.string.FavoriteSectionTitle)));
        sections.add(new SimpleSectionedRecyclerViewAdapter.Section(favoriteContactList.size(),getString(R.string.OtherSectionTitle)));
        SimpleSectionedRecyclerViewAdapter sectionedAdapter = new SimpleSectionedRecyclerViewAdapter(this, R.layout.section,R.id.section_text,contactAdapter);
        sectionedAdapter.setSections(sections.toArray(new SimpleSectionedRecyclerViewAdapter.Section[sections.size()]));
        recyclerView.setAdapter(sectionedAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(lazyRefresh.get()) {
            sortContacts();
            contactAdapter.notifyDataSetChanged();
            defineSections(getFavorites(contactList));
        }
    }

    private List<Contact> getFavorites(List<Contact> contactList) {
        return contactList.stream().filter(c -> c.getIsFavorite()).collect(Collectors.toList());
    }

    public static void setLazyRefresh() {
        lazyRefresh.set(true);
    }
}
