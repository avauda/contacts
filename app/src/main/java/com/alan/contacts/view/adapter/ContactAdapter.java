package com.alan.contacts.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alan.contacts.R;
import com.alan.contacts.model.Contact;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import io.github.rockerhieu.emojicon.EmojiconTextView;

public class ContactAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Contact> contactList;
    Context context;
    ContactClickListener listener;


    public ContactAdapter(Context context, List<Contact> contactList, ContactClickListener listener) {
        this.context = context;
        this.contactList = contactList;
        this.listener = listener;
    }

    public interface ContactClickListener {
        void onItemClick(Contact contact);
    }

    static class ContactViewHolder extends RecyclerView.ViewHolder {
        TextView nameTv, companyTv;
        SimpleDraweeView profilePictureDv;
        EmojiconTextView favStar;

        ContactViewHolder(View view) {
            super(view);
            nameTv = view.findViewById(R.id.name_tv);
            companyTv = view.findViewById(R.id.company_tv);
            profilePictureDv = view.findViewById(R.id.profile_picture);
            favStar = view.findViewById(R.id.favStar);
            view.setTag(this);
        }

        public void bind(final Contact contact, final ContactClickListener listener) {
            if (contact.getIsFavorite()) {
                favStar.setVisibility(View.VISIBLE);
            } else {
                favStar.setVisibility(View.GONE);
            }
            nameTv.setText(contact.getName());
            companyTv.setText(contact.getCompanyName());
            profilePictureDv.setImageURI(contact.getSmallImageURL());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(contact);
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.agenda_item, parent, false));
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ContactViewHolder)holder).bind(contactList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }
}
