package com.alan.contacts.view.activity;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alan.contacts.R;
import com.alan.contacts.model.Address;
import com.alan.contacts.model.Contact;
import com.alan.contacts.model.Displayable;
import com.alan.contacts.model.Phone;
import com.alan.contacts.utils.DateUtils;
import com.alan.contacts.utils.StringUtils;
import com.alan.contacts.viewmodel.ContactsViewModel;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;

import java.lang.reflect.Field;

public class ContactDetailsActivity extends AppCompatActivity {

    private Contact contact;
    private LinearLayout personalInformationLl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);

        ContactsViewModel contactsViewModel = new ContactsViewModel();

        contact = new Gson().fromJson(getIntent().getExtras().getString("contact"), Contact.class);

        configureActionBar(contactsViewModel);

        personalInformationLl = (LinearLayout) findViewById(R.id.personal_information_ll);
        SimpleDraweeView profilePicture = (SimpleDraweeView) findViewById(R.id.profile_picture);

        //Name
        ((TextView) findViewById(R.id.name_tv)).setText(contact.getName());
        //Company Name}
        ((TextView) findViewById(R.id.company_tv)).setText(contact.getCompanyName());
        //Display Profile Picture
        profilePicture.setImageURI(contact.getLargeImageURL());
        //Set up phone information
        addPhoneNumbers();
        //Set up extra information
        addAddress();
        addBirthDate();
        addEmail();
    }

    private void configureActionBar(final ContactsViewModel contactsViewModel) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        View actionBarDetails = getLayoutInflater().inflate(R.layout.actionbar_contact_details, null, false);
        Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.lightBlue), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        ImageView favStar = actionBarDetails.findViewById(R.id.favStar);
        if(contact.getIsFavorite()) {
            favStar.setImageResource(R.mipmap.ic_favorite_true);
        } else {
            favStar.setImageResource(R.mipmap.ic_favorite_false);
        }
        favStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactsViewModel.toggleFavorite(contact);
                if(contact.getIsFavorite()) {
                    favStar.setImageResource(R.mipmap.ic_favorite_false);
                } else {
                    favStar.setImageResource(R.mipmap.ic_favorite_true);
                }
                AgendaActivity.setLazyRefresh();
            }
        });
        getSupportActionBar().setCustomView(actionBarDetails);
    }

    private void addPhoneItem(String phoneNumber, String category) {
        View item;
        item = getLayoutInflater().inflate(R.layout.contact_details_phone_item, null, false);
        ((TextView) item.findViewById(R.id.phone_number_tv)).setText(phoneNumber);
        ((TextView) item.findViewById(R.id.phone_type_tv)).setText(category);
        personalInformationLl.addView(item);
        addSeparatorLine();
    }

    private void addItem(String title, String value) {
        View item = getLayoutInflater().inflate(R.layout.contact_details_item, null, false);
        ((TextView) item.findViewById(R.id.title_tv)).setText(title);
        ((TextView) item.findViewById(R.id.value_tv)).setText(value);
        personalInformationLl.addView(item);
        addSeparatorLine();
    }

    private void addSeparatorLine() {
        View separator = new View(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, 1);
        separator.setBackgroundColor(getColor(R.color.softGrey));
        int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics());
        layoutParams.setMargins(margin, 0, margin, 0);
        personalInformationLl.addView(separator, layoutParams);
    }

    private void addPhoneNumbers() {
        Phone phone = contact.getPhone();
        if(StringUtils.isValid(phone.getHome())) {
            addPhoneItem(phone.getHome(), "Home");
        }
        if(StringUtils.isValid(phone.getMobile())) {
            addPhoneItem(phone.getMobile(), "Mobile");
        }
        if(StringUtils.isValid(phone.getWork())) {
            addPhoneItem(phone.getWork(), "Work");
        }
    }

    private void addAddress() {
        if(contact.getAddress() != null) {
            addItem("Address", contact.getAddress().display());
        }
    }

    private void addBirthDate() {
        if(contact.getBirthdate() != null) {
            addItem("Birthdate", DateUtils.parseDate(contact.getBirthdate(), "MMM d, yyyy"));
        }
    }

    private void addEmail() {
        if(contact.getEmailAddress() != null) {
            addItem("Email", contact.getEmailAddress());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    //Only here to discuss later
    private void reflectionMethod() {
        //The problem encountered this way was handling with the Birthdate and the Email
        for (Field field : contact.getClass().getDeclaredFields()) {
            //field.setAccessible(true); // if you want to modify private fields
            View item = null;
            field.setAccessible(true);
            if(field.getType() == Phone.class) {
                for (Field phoneField : contact.getPhone().getClass().getDeclaredFields()) {
                    try {
                        item = getLayoutInflater().inflate(R.layout.contact_details_phone_item, null, false);
                        ((TextView) item.findViewById(R.id.phone_number_tv)).setText((String) phoneField.get(contact));
                        ((TextView) item.findViewById(R.id.phone_type_tv)).setText(phoneField.getName());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            else if(field.getType() == Address.class) {
                try {
                    item = getLayoutInflater().inflate(R.layout.contact_details_item, null, false);
                    ((TextView) item.findViewById(R.id.title_tv)).setText(((Displayable) field.get(contact)).display());
                    ((TextView) item.findViewById(R.id.value_tv)).setText(field.getName());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            else {
                try {
                    item = getLayoutInflater().inflate(R.layout.contact_details_item, null, false);
                    ((TextView) item.findViewById(R.id.title_tv)).setText(field.getName());
                    ((TextView) item.findViewById(R.id.value_tv)).setText(StringUtils.capitalize((String)field.get(contact)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            if(item != null) {
                personalInformationLl.addView(item);
                item = null;
            }
        }
    }
}
