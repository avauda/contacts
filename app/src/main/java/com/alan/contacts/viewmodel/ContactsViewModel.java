package com.alan.contacts.viewmodel;


import com.alan.contacts.application.Session;
import com.alan.contacts.model.Contact;

import java.util.List;

public class ContactsViewModel {

    public void toggleFavorite(Contact contact) {
        if(Session.getSession().getContactList().contains(contact)) {
            List<Contact> contactList = Session.getSession().getContactList();
            Contact selectedContact = contactList.get(contactList.indexOf(contact));
            selectedContact.setIsFavorite(!selectedContact.getIsFavorite());
        }
    }
}
