package com.alan.contacts.application;

import com.alan.contacts.model.Contact;

import java.util.List;

public class Session {

    //TODO: Normally would use SharedPref, but no persistence is needed.
    private List<Contact> contactList;
    private static Session instance;

    public static Session getSession() {
        if(instance == null) {
            instance = new Session();
        }
        return instance;
    }

    public void setContacts(List<Contact> contactList) {
        this.contactList = contactList;
    }

    public List<Contact> getContactList() {
        return contactList;
    }

}