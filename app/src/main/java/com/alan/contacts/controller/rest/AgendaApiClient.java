package com.alan.contacts.controller.rest;

import com.alan.contacts.controller.rest.AgendaApiInterface;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AgendaApiClient {

    private static AgendaApiInterface agendaApiInterface;

    public static AgendaApiInterface getClient() {
        if (agendaApiInterface == null) {
            OkHttpClient okClient = new OkHttpClient.Builder()
                    .readTimeout(2, TimeUnit.SECONDS)
                    .connectTimeout(2, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true).build();
            Retrofit client = new Retrofit.Builder()
                    .client(okClient)
                    .baseUrl("https://s3.amazonaws.com/technical-challenge/v3/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            agendaApiInterface = client.create(AgendaApiInterface.class);
        }
        return agendaApiInterface;
    }

}
