package com.alan.contacts.controller.rest;

import com.alan.contacts.model.Contact;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AgendaApiInterface {

    @GET("contacts.json")
    public Call<List<Contact>> getContacts();
}
