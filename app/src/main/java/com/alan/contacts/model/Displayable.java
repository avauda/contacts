package com.alan.contacts.model;


public interface Displayable {
    public String display();
}
