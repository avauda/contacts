package com.alan.contacts.model;

import com.facebook.common.internal.Objects;

import java.util.Comparator;

public class Contact {

    private String name;
    private String id;
    private String companyName;
    private Boolean isFavorite;
    private String smallImageURL;
    private String largeImageURL;
    private String emailAddress;
    private String birthdate;
    private Phone phone;
    private Address address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getSmallImageURL() {
        return smallImageURL;
    }

    public void setSmallImageURL(String smallImageURL) {
        this.smallImageURL = smallImageURL;
    }

    public String getLargeImageURL() {
        return largeImageURL;
    }

    public void setLargeImageURL(String largeImageURL) {
        this.largeImageURL = largeImageURL;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public static Comparator<Contact> COMPARE_BY_NAME = new Comparator<Contact>() {
        public int compare(Contact one, Contact other) {
            return one.name.compareTo(other.name);
        }
    };

    public static Comparator<Contact> COMPARE_BY_FAVORITE = new Comparator<Contact>() {
        public int compare(Contact one, Contact other) {
            return other.isFavorite.compareTo(one.isFavorite);
        }
    };

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) { return false; }
        Contact other = (Contact) obj;
        return Objects.equal(this.name, other.name)
                && Objects.equal(this.companyName, other.companyName)
                && Objects.equal(this.isFavorite, other.isFavorite)
                && Objects.equal(this.smallImageURL, other.smallImageURL)
                && Objects.equal(this.largeImageURL, other.largeImageURL)
                && Objects.equal(this.emailAddress, other.emailAddress)
                && Objects.equal(this.birthdate, other.birthdate)
                && Objects.equal(this.phone, other.phone)
                && Objects.equal(this.address, other.address);
    }
}