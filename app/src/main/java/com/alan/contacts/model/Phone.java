package com.alan.contacts.model;

import com.facebook.common.internal.Objects;

public class Phone {

    private String work;
    private String home;
    private String mobile;

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) { return false; }
        Phone other = (Phone) obj;
        return Objects.equal(this.work, other.work)
                && Objects.equal(this.home, other.home)
                && Objects.equal(this.mobile, other.mobile);
    }
}