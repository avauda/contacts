package com.alan.contacts.model;


import com.facebook.common.internal.Objects;

public class Address implements Displayable {

    private String street;
    private String city;
    private String state;
    private String country;
    private String zipCode;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) { return false; }
        Address other = (Address) obj;
        return Objects.equal(this.street, other.street)
                && Objects.equal(this.city, other.city)
                && Objects.equal(this.state, other.state)
                && Objects.equal(this.country, other.country)
                && Objects.equal(this.zipCode, other.zipCode);
    }

    @Override
    public String display() {
        return street + " " + city + ", " + state + " " + zipCode + "," + country;
    }
}