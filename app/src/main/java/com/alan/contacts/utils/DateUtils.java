package com.alan.contacts.utils;


import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class DateUtils {

    public static String parseDate(String input, String outputFormat){
        String pattern = "yyyy-mm-dd";
        DateTime dateTime  = DateTime.parse(input, DateTimeFormat.forPattern(pattern));
        return dateTime.toString(outputFormat);
    }
}
