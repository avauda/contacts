package com.alan.contacts.utils;


public class StringUtils {
    public static String capitalize(String word) {
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }
    public static boolean isValid(String word) {
        return word != null && !word.isEmpty();
    }
}
